# ScoutMasters CS4604 Project - Question 1

## Project Description
The Scoutmaster for a local Scout troop would like to track which scouts have attended scouting trips. Each trip occurs on a specific date and at a specific location. Some trips require a minimum age or scout rank in order to attend.

Each scout has a name, date of birth and rank.

Develop a system to track scouts, trips and which scouts have attended each trip.

## Entity Relationship

The entities in this project are a *ScoutTrip* and a *Scout*. A Scout *Attends* a ScoutTrip. 

A ScoutTrip is uniquely identified by *Id* or the *TripDate*.
A Scout is uniquely identified by *Id* or the *Name*.
Many Scouts Attend many ScoutTrips.

![ER Diagram](diagrams/entity-relationship.png  "ER Diagram")

## Relational

The diagram below shows the entity-relationship diagram converted to a relational schema.

![Relational Diagram](diagrams/relational.png "Relational Diagram")

The corresponding sql can be found [here](sql/install.sql).

## Build/Install/Run

### Docker Playground

Go to the [Docker Playground](http://play-with-docker.com) and confirm that you are not a robot.  
In the left-hand menu, click on "ADD NEW INSTANCE"  
This will start a Docker instance (node) and log you in as the root user. 

#### Start Postgresql server

`docker run -d --name postgres -e POSTGRES_HOST_AUTH_METHOD=trust postgres -c 'log_min_duration_statement=0'`

#### Create the schema

```
curl https://code.vt.edu/rquintin/cs4604-spr20-project.2/raw/master/project2/scenario-1/sql/install.sql > install.sql
docker run -it --rm -v $(pwd)/install.sql:/install.sql --link postgres:postgres postgres psql -a -h postgres -U postgres -f /install.sql
```

#### Load data

```
curl https://code.vt.edu/rquintin/cs4604-spr20-project.2/raw/master/project2/scenario-1/sql/load.sql > load.sql
docker run -it --rm -v $(pwd)/load.sql:/load.sql --link postgres:postgres postgres psql -a -h postgres -U postgres -f /load.sql
```

#### Browse the database

`docker run -it --rm --link postgres:postgres postgres psql -a -h postgres -U postgres`

Use `\d` to see the list of available relations.

